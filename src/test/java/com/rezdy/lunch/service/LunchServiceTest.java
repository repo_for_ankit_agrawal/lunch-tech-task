package com.rezdy.lunch.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.junit.jupiter.SpringExtension;

//@ExtendWith(SpringExtension.class)
//@SpringBootTest
public class LunchServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(LunchServiceTest.class);
	
	@Autowired
	private LunchService lunchService;
	
	@Test
	public void testLoadRecipes() {
		LocalDate date = LocalDate.parse("2020-10-01");
		try {
			List<Recipe> recipes = lunchService.loadRecipes(date);
			
			assertEquals(5,recipes.size());
		}catch(Exception ex){
			logger.error("Exception in testLoadRecipes"+ex);
		}
	}
	
	@Test
	public void testsortRecipes() {
		LocalDate date = LocalDate.parse("2020-10-01");
		try {
			List<Recipe> recipes = lunchService.getNonExpiredRecipesOnDate(date);
			assertEquals(3,recipes.size());
		}catch(Exception ex){
			logger.error("Exception in lunch service test"+ex);
		}
	}
		

}
