package com.rezdy.lunch.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URL;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class LunchControllerTest {

	private static final Logger logger = LoggerFactory.getLogger(LunchControllerTest.class);
	
	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void testGetRecipes() {
		try {
			ResponseEntity<?> response = restTemplate.getForEntity(
					new URL("http://localhost:"+port+"/lunch?date=2020-10-10").toString(), String.class);
			assertEquals("200 OK",response.getStatusCode().toString());
		}catch(Exception ex) {
			logger.error("Error in testGetRecipes:"+ex);
		}
		
	}
	
	@Test
	public void testGetRecipesError() {
		try {
			ResponseEntity<?> response = restTemplate.getForEntity(
					new URL("http://localhost:"+port+"/lunch?date=2020/10/10").toString(), String.class);
			assertEquals(HttpStatus.INTERNAL_SERVER_ERROR,response.getStatusCode());
		}catch(Exception ex) {
			logger.error("Error in testGetRecipesError"+ex);
		}
		
	}
}
