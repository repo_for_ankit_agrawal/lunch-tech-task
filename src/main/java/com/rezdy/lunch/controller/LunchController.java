package com.rezdy.lunch.controller;

import com.rezdy.lunch.service.LunchService;
import com.rezdy.lunch.service.Recipe;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import com.rezdy.lunch.service.Recipe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.LocalDate;
import java.util.List;
//import java.util.List;

@RestController
public class LunchController {

	private static final Logger logger = LoggerFactory.getLogger(LunchController.class);
	
    private LunchService lunchService;

    @Autowired
    public LunchController(LunchService lunchService) {
        this.lunchService = lunchService;
    }

    //@PostMapping("/lunch")
    @RequestMapping(value = "/lunch", method = RequestMethod.GET)
    public List<Recipe> getRecipes(@RequestParam(value = "date") String date) throws Exception{
        return lunchService.getNonExpiredRecipesOnDate(LocalDate.parse(date));
    }
    
//    @RequestMapping(value = "/lunch", method = RequestMethod.GET)
//    public ResponseEntity<?> getRecipes(@RequestParam(value = "date") String date) {
//    	try {
//    		return new ResponseEntity<>(lunchService.getNonExpiredRecipesOnDate(LocalDate.parse(date)),HttpStatus.OK);
//    	}catch(Exception ex) {
//    		logger.error("Error while fetching list of recipes:"+ex);
//    		return new ResponseEntity<>(ex.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
//    	}
//       
//    }
}
