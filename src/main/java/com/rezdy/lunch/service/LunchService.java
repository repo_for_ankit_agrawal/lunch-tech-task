package com.rezdy.lunch.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

@Service
public class LunchService {

	private static final Logger logger = LoggerFactory.getLogger(LunchService.class);
			
    @Autowired
    private EntityManager entityManager;

    private List<Recipe> recipesSorted;

    public List<Recipe> getNonExpiredRecipesOnDate(LocalDate date) throws Exception{
    	
    	List<Recipe> recipes = loadRecipes(date);
    	
        sortRecipes(recipes,date);

        return recipesSorted;
    	
    }

    private void sortRecipes(List<Recipe> recipes, LocalDate date) {
    	Map<LocalDate,List<Recipe>> recipeBestBefore = new TreeMap<LocalDate,List<Recipe>>(Collections.reverseOrder());
    	
    	try {
    		for(Recipe r : recipes) {
        		boolean containsExpiredIngredient = false;
        		Set<Ingredient> temp = r.getIngredients();
        		Iterator<Ingredient> itr = temp.iterator();

        		LocalDate dd = itr.next().getBestBefore();
        		
        		while(itr.hasNext()) {
        			Ingredient ing = (Ingredient)itr.next();	
        			if(ing.getUseBy().compareTo(date) < 0) {
        				containsExpiredIngredient = true;
        				break;
        			}
        			
        			if(ing.getBestBefore().isBefore(date)) {
        				dd = ing.getBestBefore();
        			}
        		}
        		
        		if(!containsExpiredIngredient) {
        			if(!recipeBestBefore.containsKey(dd))
        				recipeBestBefore.put(dd, new ArrayList<Recipe>());
        			List<Recipe>recipeList = recipeBestBefore.get(dd);
        			recipeList.add(r);
               		recipeBestBefore.put(dd,recipeList);
        		}    		
        	}
    		
        	recipes.clear();
        	for(LocalDate keyDate : recipeBestBefore.keySet()) {
        		recipes.addAll(recipeBestBefore.get(keyDate));
        	}
            recipesSorted = recipes; //TODO sort recipes considering best-before
    	}catch(Exception ex) {
    		logger.error("Error when sorting recipes in LunchService:"+ex);
    		throw new RuntimeException("Error while sorting recipes:"+ex.getMessage());
    	}
    }

    public List<Recipe> loadRecipes(LocalDate date) throws Exception{
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Recipe> criteriaQuery = cb.createQuery(Recipe.class);
        Root<Recipe> recipeRoot = criteriaQuery.from(Recipe.class);

        CriteriaQuery<Recipe> query = criteriaQuery.select(recipeRoot);
                
        Subquery<Recipe> nonExpiredIngredientSubquery = query.subquery(Recipe.class);
        Root<Recipe> nonExpiredIngredient = nonExpiredIngredientSubquery.from(Recipe.class);
        nonExpiredIngredientSubquery.select(nonExpiredIngredient);

        Predicate matchingRecipe = cb.equal(nonExpiredIngredient.get("title"), recipeRoot.get("title"));
        //Predicate expiredIngredient = cb.lessThan(nonExpiredIngredient.join("ingredients").get("useBy"), date);
        Predicate expiredIngredient = cb.greaterThan(nonExpiredIngredient.join("ingredients").get("useBy"), date);
        
        Predicate allNonExpiredIngredients = cb.exists(nonExpiredIngredientSubquery.where(matchingRecipe, expiredIngredient));
        
        return entityManager.createQuery(query.where(allNonExpiredIngredients)).getResultList();
    }

}